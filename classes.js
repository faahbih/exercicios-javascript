// aqui há duas classes, person circle
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Vamos criar bob novamente, usando nosso construtor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 35);

// crie sua própria classe aqui

function Circle(radius){
 this.radius = radius;   
}



//-adicionando metodos aos objtos--------------------------------------------------------
// neste caso foi necessário criar um metodo pra cada um 
function Dog (breed) {
  this.breed = breed;
}

// aqui criamos buddy e o ensinamos a latir
var buddy = new Dog("Golden Retriever");
buddy.bark = function() {
  console.log("Au");
};
buddy.bark();

// aqui criamos snoopy
var snoopy = new Dog("Beagle");
// precisamos que voce ensine snoopy a latir aqui
snoopy.bark = function(){
    console.log("Wow");
}
// isso causa um erro, porque snoopy nao sabe latir!
snoopy.bark();

//--------------------------------------------------------------------------------------
// já neste caso nao foi necessario cria um metodo pra cada um
// pois foi inserido uma função prototype

function Dog (breed) {
  this.breed = breed;
}

// aqui criamos "buddy" e o ensinamos a latir
var buddy = new Dog("golden Retriever");
Dog.prototype.bark = function() {
  console.log("Au");
};
buddy.bark();

// aqui nós criamos "snoopy"
var snoopy = new Dog("Beagle");
/// dessa vez funciona!
snoopy.bark();


//-----------------------------------------------------------------------------------///////////////////////
//mais um exemplo 

function Cat(name, breed) {
    this.name = name;
    this.breed = breed;
}

// criar algusn gatos!
var cheshire = new Cat("Cheshire", "Britanico de pelo curto");
var gary = new Cat("Gary", "Domestico de pelo curto");

// metodo "meow" a classe Cat que permitirá a todos os gatos exibir "Meow!" no console
Cat.prototype.meow = function() {
  console.log("Miau!");
}
// código fazer os gatos miarem!
cheshire.meow();
gary.meow();


//---exemplo de introdução a herança---------------------------------------------------/////////////////////
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Oi, meu nome é " + this.name);
};


// crie aqui um construtor Penguin
function Penguin(name, numLegs){
    this.name = name;
    this.numLegs = numLegs;    
}

// crie aqui um metodo sayName para Penguins
Penguin.prototype.sayName = function(){
    console.log("Oi, meu nome é " + this.name);
}


// our test code
var theCaptain = new Penguin("Capitão Cook", 2);
theCaptain.sayName();

//--------- herança ocorre na ultima linha --------------------------------------------//
// a classe Animal e método sayName originais
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Oi, meu nome é" + this.name);
};

// defina uma classe Penguin
function Penguin(name){
    this.name = name;
    this.numLegs = 2;
}


// faca seu protótipo ser uma nova instância de Animal
Penguin.prototype = new Animal();


// outro exemplo de herança --------------------------------------------------------//
function Penguin(name) {
    this.name = name;
    this.numLegs = 2;
}

// crie aqui sua classe Emperor e faca-a herdar de Penguin

function Emperor(name){
    this.name = name;
}
Emperor.prototype = new Penguin(); // herdando de penguin



// crie um objeto "emperor" e exiba o numero de pernas que ele tem
var emperor = new Emperor("bobo");
console.log(emperor.numLegs);