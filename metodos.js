// review de metodos

var bob = new Object();
bob.age = 17;
// dessa vez adicionamos um metodo, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};

//retorna quando bob nasceu
bob.getYearOfBirth = function () {
  return 2014 - bob.age;
};
console.log(bob.getYearOfBirth());



// aqui definimos nosso metodo usando "this", antes mesmo de apresentarmos bob ------------------------------------////////////
var setAge = function (newAge) {
  this.age = newAge;
};
// agora criamos bob
var bob = new Object();
bob.age = 30;
// e aqui simplesmente usamos o metodo que ja criamos
bob.setAge = setAge;
  
// muda a idade de bob para 50
bob.setAge(50);




// exemplo de metodo alterando altura e largura do rectangle  ------------------------------------------///////////////////////////
var rectangle = new Object();
rectangle.height = 3;
rectangle.width = 4;
// aqui esta nosso metodo para definir a altura
rectangle.setHeight = function (newHeight) {
  this.height = newHeight;
};
// ajude finalizando este metodo
rectangle.setWidth = function(newWidth){
    this.width = newWidth;
};  

// aqui, mude a largura para 8 e a altura para 6 usando nossos novos metodos
rectangle.setHeight(6);
rectangle.setWidth(8);



//exemplo que calcula area e perimetro -----------------------------------------------------/////////////////////////////////////
var square = new Object();
square.sideLength = 6;
square.calcPerimeter = function() {
  return this.sideLength * 4;
};
// nos ajude a definir um método de area aqui
square.calcArea = function(){
    return this.sideLength * this.sideLength;
}

var p = square.calcPerimeter();
var a = square.calcArea();



//construtor personalizados  -----------------------------------------------------------///////////////////////////////////
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Vamos criar bob e susan novamente, usando nosso construtor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 25);
// ajude-nos a criar george, cujo nome e "George Washington" e idade e 275
var george = new Person("George Washington", 275);



// construtores com metodos --------------------------------------------------------//////////////////////////////////
function Rectangle(height, width) {
  this.height = height;
  this.width = width;
  this.calcArea = function() {
      return this.height * this.width;
  };
  // coloque sua função de cálculo do perimetro aqui!
  this.calcPerimeter = function(){
      return this.height * 2 + this.width * 2;
  }
}

var rex = new Rectangle(7,3);
var area = rex.calcArea();
var perimeter = rex.calcPerimeter();


//outro exemplo --------------------------------------------------------------------------/////////////////////////////
function Rabbit(adjective) {
    this.adjective = adjective;
    this.describeMyself = function() {
        console.log("Eu sou um " + this.adjective + " coelho");
    };
}

// agora podemos fazer facilmente todos os nossos coelhos
var rabbit1 = new Rabbit("fofinho");
var rabbit2 = new Rabbit("alegre");
var rabbit3 = new Rabbit("sonolento");



//----------- mais exemplo de metodos --------------------------------------------------/////////////////////////
function Circle (radius) {
    this.radius = radius;
    this.area = function () {
        return Math.PI * this.radius * this.radius;
        
    };
    // defina um método para cálculo do perimetro aqui
    this.perimeter = function(){
        return 2 * Math.PI * this.radius;
    } ;  
}



///////////////////exemplo de construtor person usando uma função dentro do corpo //////////////////////////////
function Person(job, married) {
    this.job = job;
    this.married = married;
    
    // adicione um metodo "speak" a Person!
    this.speak = function(){
        console.log("Alô!");
    }
    
}

var user = new Person("Aluno da Codecademy",false);
user.speak();


//exemplo passando uma função em objetos de notação literal ----------------------------------------------------//////
var james = {
    job: "programador",
    married: false,
    speak: function(mood) {
        console.log("Alô, estou me sentindo ótimo");
        console.log("Alô, estou me sentindo bem");
        
    }
};

james.speak("ótimo");
james.speak("bem");

//--------- alterando o trabalho de james -------------------------------------------------////////////////////////
var james = {
    job: "programador",
    married: false,
    sayJob: function() {
        // complete este metodo
        console.log("Oi eu sou um " + job);
        
    }
};

// primeiro emprego de james
james.sayJob();

// mude o emprego de james para "super programador" aqui
james.job = "super programador"

// segundo emprego de james
james.sayJob();


//-verifica se no meu objeto tem determinada propriedade (chave:valor)-------------------------///////////////////

var myObj = {
    // preencha com propriedades myObj
    name :"teste"
    
};

console.log( myObj.hasOwnProperty('name') ); // deve exibir true
console.log( myObj.hasOwnProperty('nickname') ); // deve exibir false porque eu não tenho no meu construtor


//---exemplo de hasOwnProperty --------------------------------------------------------------------/////////////////////////////

var suitcase = {
    shirt: "Hawaiano"
};

console.log(suitcase.shorts);

if((suitcase.hasOwnProperty('shorts'))){
    console.log("shorts");
}else{
    console.log("n");
  }

//-listando as propriedades de um objeto ---------------------------------------------------------/////////////////
var nyc = {
    fullName: "Nova York",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

for(var property in nyc ){
    console.log(property);
}

// listando os valores das propriedades de um obj ------------------------------------------------/////////////////
var nyc = {
    fullName: "Nova York",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

// escreva um laço for-in para exibir o valor das propriedades de nyc
for(var property in nyc){
    console.log(nyc[property]);
}