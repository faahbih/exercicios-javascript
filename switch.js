// exemplo simples de escolha

var lunch = prompt("O que voce quer para o almoco?","Digite sua opcao de almoco aqui");

switch(lunch){
  case 'sanduíche':
    console.log("Claro! Um sanduíche saindo.");
    break;
  case 'sopa':
    console.log("Entendi! A de tomate e minha favorita.");
    break;
  case 'salada':
    console.log("Parece bom! Que tal uma salada cesar?");
    break;
  case 'torta':
    console.log("Uma torta nao e uma refeicao!");
    break;
  default:
    console.log("Não tenho certeza do que e " + lunch + ". Que tal um sanduíche?");
}



// exemplo 2

var color = prompt("Qual e sua cor primaria favorita?","Digite sua cor favorita aqui");

switch(color) {
  case 'vermelho':
    console.log("Vermelho é uma boa cor!");
    break;
  case 'azul':
    console.log("Essa é minha cor favorita tambem!");
    break;
  //Adicione seu caso aqui!
  case 'amarelo':
      console.log("Essa é a cor do sol!");
      break;
  
  default:
    console.log("Eu nao acho que essa seja uma cor primária!");
}


// exemplo 3

var getReview = function (movie) {
    
    switch(movie){
        case 'Toy Story 2':
            return "Grande história. Mineiro ruim."
            break;
        case 'Procurando Nemo':
            return "Animação legal e tartarugas engraçadas."
            break;
        case 'O Rei Leão':
           return "Grandes músicas."
            break;
            
        default:
            return "Eu não sei!"
    }
};
getReview("O Rei Leão");