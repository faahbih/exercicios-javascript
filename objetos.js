// criando e salvando informações de um objeto

// outra forma de se criar um objeto (1)
var bob = {
  name: "Bob Smith",
  age: 30
};
var susan = {
  name: "Susan Jordan",
  age: 25
};

// acessando propriedade
var name1 = bob.name;
var age1 = bob.age;

var name2 = susan.name;
var age2 = susan.age;



// outra forma de se criar um objeto (2)

var dog = {
  species: "sabujo",
  weight: 60,
  age: 4
};

var species = dog["species"];
var weight = dog["weight"];
var age = dog["age"];


// outra forma de se criar um objeto (3)

// Seu objeto bob novamente, mas criado usando um construtor dessa vez 
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;


var susan2 = new Object();
susan2.name = "Susan Jordan";
susan2.age = 24;



//construtor personalizados para criar varios objetos
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Vamos criar bob e susan novamente, usando nosso construtor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 25);
// ajude-nos a criar george, cujo nome e "George Washington" e idade e 275
var george = new Person("George Washington", 275);



function Person(name,age) {
  this.name = name;
  this.age = age;
  this.species = "Homo Sapiens";
}

var sally = new Person("Sally Bowles", 39);
var holden = new Person("Holden Caulfield", 16);
console.log("a espécie de sally é " + sally.species + " e a idade dela é " + sally.age);
console.log("a espécie de holden é " + holden.species + " e a idade dele é " + holden.age);