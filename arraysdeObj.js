// Nosso construtor person
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// Agora, podemos fazer um array de pessoas
var family = new Array();
family[0] = new Person("alice", 40);
family[1] = new Person("bob", 42);
family[2] = new Person("michelle", 8);
family[3] = new Person("timmy",6);


// percorra o novo array
for(var i=0; i < family.length; i++){
    console.log(family[i].name);
}



//PASSANDO OBJETOS PARA FUNÇÕES

// Nosso construtor person
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// Podemos fazer uma função que toma pessoas como argumentos
// Esta computa a diferença de idade entre duas pessoas
var ageDifference = function(person1, person2) {
    return person1.age - person2.age;
}

var alice = new Person("Alice", 30);
var billy = new Person("Billy", 25);

// obtenha a diferença de idade entre alice e billy usando nossa função
var diff = ageDifference(alice,billy);


//PASSANDO OBJETOS PARA FUNÇÕES pt 2

// Nosso construtor person
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// Podemos fazer uma função que toma pessoas como argumentos
// Esta computa a diferença de idades entre duas pessoas
var ageDifference = function(person1, person2) {
    return person1.age - person2.age;
};

// Faça uma nova função, olderAge, para retornar a idade da
// mais velha das duas pessoas
var olderAge = function(person1,person2){
    if(person1.age > person2.age){
        return person1.age
    }
    else{
        return person2.age
    }
}


// Vamos trazer alice e billy de volta para testar nossa nova função
var alice = new Person("Alice", 30);
var billy = new Person("Billy", 25);

console.log("A idade da pessoa mais velha é " + olderAge(alice, billy));


//exibindo pessoas
// função que mostra o primeiro e o ultimo nome
var bob = {
    firstName: "Bob",
    lastName: "Jones",
    phoneNumber: "(650) 777-7777",
    email: "bob.jones@example.com"
};

var mary = {
    firstName: "Mary",
    lastName: "Johnson",
    phoneNumber: "(650) 888-8888",
    email: "mary.johnson@example.com"
};

var contacts = [bob, mary];

// printPerson added here
var printPerson = function(person){
    console.log(person.firstName + " " + person.lastName);
}

printPerson(contacts[0]);
printPerson(contacts[1]);


//listando 

var bob = {
    firstName: "Bob",
    lastName: "Jones",
    phoneNumber: "(650) 777-7777",
    email: "bob.jones@example.com"
};

var mary = {
    firstName: "Mary",
    lastName: "Johnson",
    phoneNumber: "(650) 888-8888",
    email: "mary.johnson@example.com"
};

var contacts = [bob, mary];

function printPerson(person) {
    console.log(person.firstName + " " + person.lastName);
}

var list = function(){
    var contactsLength = contacts.length;
    for(var i = 0; i < contactsLength; i++)
    {
        console.log(printPerson(contacts[i]));
    }
}
list();




//função de busca

var bob = {
    firstName: "Bob",
    lastName: "Jones",
    phoneNumber: "(650) 777-7777",
    email: "bob.jones@example.com"
};

var mary = {
    firstName: "Mary",
    lastName: "Johnson",
    phoneNumber: "(650) 888-8888",
    email: "mary.johnson@example.com"
};

var contacts = [bob, mary];

function printPerson(person) {
    console.log(person.firstName + " " + person.lastName);
}

function list() {
	var contactsLength = contacts.length;
	for (var i = 0; i < contactsLength; i++) {
		printPerson(contacts[i]);
	}
}

/*Crie uma funcao search
e chame-a pasando "Jones"*/

function search(lastName){
    var contactsLength = contacts.length;
    for(var i=0; i < contactsLength; i++){
        if(contacts[i].lastName === lastName)
        {
            printPerson(contacts[i]);
        }
    }
}

search("Jones");


////////////////// adicionando um novo contato  //////////////
var bob = {
    firstName: "Bob",
    lastName: "Jones",
    phoneNumber: "(650) 777-7777",
    email: "bob.jones@example.com"
};

var mary = {
    firstName: "Mary",
    lastName: "Johnson",
    phoneNumber: "(650) 888-8888",
    email: "mary.johnson@example.com"
};

var contacts = [bob, mary];

function printPerson(person) {
    console.log(person.firstName + " " + person.lastName);
}

function list() {
	var contactsLength = contacts.length;
	for (var i = 0; i < contactsLength; i++) {
		printPerson(contacts[i]);
	}
}

/*Crie uma funcao search
e chame-a pasando "Jones"*/

function search(lastName){
    var contactsLength = contacts.length;
    for(var i=0; i < contactsLength; i++){
        if(contacts[i].lastName === lastName)
        {
            printPerson(contacts[i]);
        }
    }
}

search("Jones");

function add(firstName,lastName,phoneNumber,email){
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
}

contacts[contacts.length] = new add("Fabiana", "Oliveira", "51 55555555", "fulano@codecademy")

list();
search("Fabiana");


