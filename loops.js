/* exemplos de loops em javascript*/

//1
var loopCondition = false;
  // Escreva seu laco do/while aqui!
  do {
	console.log("Vou interromper o laço porque minha condicao é " + String(loopCondition) + "!");	
} while (loopCondition);


//2
for(var i=0; i<20; i+=5){
    console.log(i);
}

//3
var teste = true;
while(teste){
    console.log("Verdade!");
    teste = false;
}


// exemplo de jogo simples de mata dragão

var slaying = true;
var youHit = Math.floor(Math.random() * 2);
var damageThisRound = Math.floor(Math.random()*5 + 1);
var totalDamage = 0;

while(slaying){
    
    if (youHit){
        console.log("Você acertou!");
        totalDamage += damageThisRound;
        if(totalDamage >= 4){
            console.log("Você venceu!");
            slaying = false;
        }
        else{
            youHit = Math.floor(Math.random() * 2);
        }

    }
    else{
        console.log("Você perdeu!");
        slaying = false;
    }
    
}