CODIGOS EM JAVASCRIPT
----------------------------------------------------------------------------------------------
//1 - NÃO PERMITE SELECIONAR TEXTO NA PAGINA
//ENTRE AS TAGS <head> </head> INSIRA O CÓDIGO ABAIXO

function disableselect(e){
return false
};
function reEnable(){
return true
}
document.onselectstart=new Function ("return false")
if (window.sidebar){
document.onmousedown=disableselect
document.onclick=reEnable
}

----------------------------------------------------------------------------------------------
//2 - MOUSE DIREITO NÃO FUNCIONA
//ENTRE AS TAGS <head> </head> INSIRA O CÓDIGO ABAIXO
<script language="javascript">
function click() {
if (event.button==2||event.button==3) {
 oncontextmenu='return false';
  }
}
document.onmousedown=click
document.oncontextmenu = new Function("return false;")
</script>

---------------------------------------------------------------------------------------------
Visualiar o diretorio atual (Explorer)
<a href="" ><b>Explorer</b></a>

Margem
<HTML>
<BODY>
  <P STYLE="margin-left: 1 mm">Margem de 1 mm</P>
  <P STYLE="margin-left: 1 cm">Margem de 1 cm</P>
  <P STYLE="margin-left: 1 in">Margem de 1 in</P>
  <P STYLE="margin-left: 1 pt">Margem de 1 pt</P>
  <P STYLE="margin-left: 1 pc">Margem de 1 pc</P>
</BODY>
</HTML>

//1- Integração com HTML
//1.1 - localização do código, inclusão de tags, o objecto document

<html>
<head>
<script language="JavaScript">
document.write ("hello world <br>");
</script>
</head>
<body>
</body>
</html>


//1.2 - informação do cliente e concatenação

<html>
<head>
<script language="JavaScript">
document.write ("Data:"+Date());
</script>
</head>
<body>
</body>
</html>

//1.3 -métodos de window; browsers sem JavaScript; variáveis

<html>
<head>
<script language="JavaScript">
<!--
var first=window.prompt("please enter name:" , "");
document.write("bem-vindo, " + first + ".<br>");
//-->
</script>
</head>
<body>
</body>
</html>

    
/2- Gestão de eventos 
Os manipuladores de eventos em javaScript são representados como atributos 
especiais que modificam o comportamento da tag HTML a que foram anexados. 
Começam todos com on e identificam diversos eventos que podem ocorrer. 

Existem dois tipos de eventos fundamentais: os eventos de sistema e os do rato. 
Eventos de sistema - exemplos: load, unload
Eventos de rato - exemplos: click, mouseover, submit.
 
2.1 - o manipulador onclick, identificação de partes do documento
Criar um formulário numa página HTML
Inserir um campo de texto com o nome "texto" e o valor "bem-vindo"
Inserir um botão de submit com o nome "press" e o valor "obrigado" 
Inserir o seguinte código na tag do botão:/

<form>
<input name="texto" value="Bem Vindo!" >
<input type="button" name="press" value="Obrigado" 
  onClick=" this.form.texto.value='de nada' " >
</form>

Impressão de página HTML
<a href="javaScript:window.print()">Imprima esta página</a>

Voltar para a página anterior
<a href="javascript:history.go(-1)">Voltar</a>

Posicionamento da página
<a href=aqui>Aqui</a>
Ao chamar a página, deve ser referenciada a posição:
<a href="pagina.htm#aqui">Pagina.HTM</a>

Confirmando uma operação

<form name=form_confirmar>
<script language=javascript>
    function Confirmar_CheckBox()
    {
        if (document.form_confirmar.check_confirmar.checked==false)
        {
        if (confirm('Esta operação limpará o checkbox. Confirma a operação ?'))
            {
            }
    else
            {
            document.form_confirmar.check_confirmar.checked=true;
            return false;
            }
        }
    }
</script>
<input type=checkbox checked name=check_confirmar OnClick="javascript:Confirmar_CheckBox();">
</form>

Trabalhando com datas

<script LANGUAGE="JavaScript">

function Dia(Data_DDMMYYYY)
{
string_data = Data_DDMMYYYY.toString();
posicao_barra = string_data.indexOf("/");
if (posicao_barra!= -1)
{
dia = string_data.substring(0,posicao_barra);
return dia;
}
else
{
return false;
}
}

function Mes(Data_DDMMYYYY)
{
string_data = Data_DDMMYYYY.toString();
posicao_barra = string_data.indexOf("/");
if (posicao_barra!= -1)
{
dia = string_data.substring(0,posicao_barra);
string_mes = string_data.substring(posicao_barra+1,string_data.length);
posicao_barra = string_mes.indexOf("/");
if (posicao_barra!= -1)
{
mes = string_mes.substring(0,posicao_barra);
mes = Math.floor(mes);
return mes;
}
else
{
return false;
}

}
else
{
return false;
}
}

function Ano(Data_DDMMYYYY)
{
string_data = Data_DDMMYYYY.toString();
posicao_barra = string_data.indexOf("/");
if (posicao_barra!= -1)
{
dia = string_data.substring(0,posicao_barra);
string_mes = string_data.substring(posicao_barra+1,string_data.length);
posicao_barra = string_mes.indexOf("/");
if (posicao_barra!= -1)
{
mes = string_mes.substring(0,posicao_barra);
mes = Math.floor(mes);
ano = string_mes.substring(posicao_barra+1,string_mes.length);
return ano;
}
else
{
return false;
}

}
else
{
return false;
}
}

function Calcula_Data(data_DDMMYYYY,dias,adicao){

Var_Dia=Dia(data_DDMMYYYY);
Var_Mes=Mes(data_DDMMYYYY);
Var_Mes=Math.floor(Var_Mes)-1;
Var_Ano=Ano(data_DDMMYYYY);

var data = new Date(Var_Ano,Var_Mes,Var_Dia);

if (adicao == true)
{
operacao = '+'
var diferenca = data.getTime() + (dias * 1000 * 60 * 60 * 24);
}
else
{
operacao = '-'
var diferenca = data.getTime() - (dias * 1000 * 60 * 60 * 24);
}
var diferenca = new Date(diferenca);
alert(string_data+operacao+dias+' dias = '+diferenca.getDate()+'/'+(parseInt(diferenca.getMonth())+1)+'/'+diferenca.getYear());

}

function Calcula_Dias(data1_DDMMYYYY,data2_DDMMYYYY){

Var_Dia1=Dia(data1_DDMMYYYY);
Var_Mes1=Mes(data1_DDMMYYYY);
Var_Mes1=Math.floor(Var_Mes1)-1;
Var_Ano1=Ano(data1_DDMMYYYY);
var data1 = new Date(Var_Ano1,Var_Mes1,Var_Dia1);

Var_Dia2=Dia(data2_DDMMYYYY);
Var_Mes2=Mes(data2_DDMMYYYY);
Var_Mes2=Math.floor(Var_Mes2)-1;
Var_Ano2=Ano(data2_DDMMYYYY);
var data2 = new Date(Var_Ano2,Var_Mes2,Var_Dia2);

var diferenca = data1.getTime() - data2.getTime();
var diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));
alert('Diferença em dias entre '+data1_DDMMYYYY+' e '+data2_DDMMYYYY+' = '+diferenca);
}

</script>

Adicionare Apagar Items de um listbox
<script language="JavaScript">
function adicionar_item()
{
if(document.form_listbox.texto.value==''){return;}

var oOption = document.createElement("OPTION");

oOption.value=document.form_listbox.texto.value;
oOption.text=document.form_listbox.texto.value;

document.form_listbox.listbox.add(oOption);

if(document.form_listbox.listbox.options[0].value=='')
{document.form_listbox.listbox.options[0] = null}

//Limpar os dados do formulário após adicionar o item ao List
document.form_listbox.texto.value='';
}

function apagar_item()
{
if (document.form_listbox.listbox.selectedIndex!=-1)
{document.form_listbox.listbox.options[document.form_listbox.listbox.selectedIndex] = null;}
}

Calculador de idade - Calcula idade a partir da data de nascimento 


Neste artigo/tutorial explicarei como criar um calculador de idade 
que calcula idade a partir da data de nascimento.

Insira o seguinte código após o <BODY>, de preferência:

<script language="Javascript">
function calcage(mm,dd,yy) {
thedate = new Date()
mm2 = thedate.getMonth() + 1
dd2 = thedate.getDate()
yy2 = thedate.getYear()
if (yy2 < 100) {
yy2 = yy2 + 1900 }
yourage = yy2 - yy
if (mm2 < mm) {
yourage = yourage - 1; }
if (mm2 == mm) {
if (dd2 < dd) {
yourage = yourage - 1; }
}
agestring = yourage + " "
document.write(agestring)
}
</script>

Insira o seguinte código onde você quer que apareça o cálculo da idade:

<script>
calcage("03", "20", "1989")
</script>

Nessa parte do código você deverá setar o mês, dia e ano (nessa ordem) 
da data de nascimento que o script fará a contagem. Como no exemplo acima,
a data de nascimento é 03/20/1989 (mês/dia/ano).


Troca de imagens 


Tenho certeza de que você foi a alguma página que um determinado link mudava 
de cor ou ganhava um efeito visual quando o mouse passava por cima.

Agora você verá um dos exemplos mais legais utilizando Javascript. com ele 
você pode trocar imagens quando o mouse passa por cima dela, sinalizando 
melhor ao visitante a existência de um link. Para facilitar o seu trabalho, 
eu preparei duas funções especiais para você usar nas suas páginas.

Para explicar melhor, nada como um bom exemplo:

<HTML>
<HEAD><TITLE>Exemplo 7</TITLE>
<SCRIPT LANGUAGE="Javascript">
navegador = navigator.appName;
versao = parseInt(navigator.appVersion);
if ((navegador == "Netscape" && versao >= 3) || versao >= 4) compativel = "sim";
else compativel = "nao";

function liga(imagem) {
if (compativel == "sim")
document.images[imagem].src = imagem + "_lig.gif";
}

function desliga(imagem) {
if (compativel == "sim")
document.images[imagem].src = imagem + ".gif";
}

</SCRIPT>
</HEAD>
<BODY>
<FONT FACE="Arial" SIZE="6" COLOR="Navy"><B>Troca de Imagens</B></FONT><BR>
<FONT FACE="Arial" SIZE="4">Posicione o mouse sobre uma imagem, <BR> e a veja mudar.</FONT><P>
<A HREF="http://www.emulatore.net" onMouseOver="liga('imagem1')" onMouseOut="desliga('imagem1')"><IMG SRC="imagem1.gif" NAME="imagem1" BORDER="0"></A>
</BODY>
</HTML>

Obviamente as imagens podem ter qualquer tamanho e você está livre para criar 
o efeito que desejar. Para possibilitar este efeito, foram introduzidos:

. Duas funções criadas em Javascript: liga e desliga;
. Dois novos parâmetros no elemento de definição de link: onMouseOver e onMouseOut;
. Um parâmetro no elemento de definição de imagem: NAME;
. Uma convenção no nome dos arquivos de imagens.

A função liga é usada no parâmetro onMouseOver, ou seja, é ativada quando o 
mouse passa por cima do link, para exibir a imagem com o efeito. Já a função 
desliga é usada para voltar a imagem ao normal. O parâmetro NAME especifica o 
nome da imagem e é indicado quando chamamos ambas as funções.

Para facilitar o seu trabalho, você deve usar imagens do tipo GIF com o mesmo 
nome usado no parâmetro NAME de identificação da imagem. Além disso, a imagem 
com efeito deve ter o nome imagem_lig.gif com "_lig" no nome, como que 
indicando o nome da imagem quando está ligada, ou seja, com efeito.

Para explicar melhor, vejamos o exemplo da figura abaixo:

<A HREF="http://www.emulatore.net" onMouseOver="liga('imagem1'" onMouseOut="desliga('imagem1')"><IMG SRC="imagem1.gif" NAME="imagem1" BORDER="0"></A>

O arquivo normal com a imagem chama-se imagem1.gif e o nome (NAME) da imagem 
é imagem1, ou seja, usamos o mesmo nome para tudo. Além disso, deve ser criada 
a imagem imagem1_lig.gif, que será mostrada quando o mouse estiver passando 
por cima.

Basicamente, você deve criar dois arquivos, imagem1.gif e imagem1_lig.gif, e 
usar o nome do arquivo sem a extensão, imagem1, como o identificador da imagem 
na página (NAME=imagem1). Se você fizer isso, basta copiar o meu exemplo, 
alterar os links (http://...) e se deliciar com um recurso tão legal. 
Não se esqueça de colocar o identificador correto nas funções liga('imagem1')
e desliga('imagem1').

Se você preferir trabalhar com imagens do tipo JPEG, basta alterar as funções 
nos locais onde tem escrito .gif para .jpg.

Apesar de parecer um tanto complicado, é mais simples do que imagina. 
Experimente fazer uma vez e você verá como é fácil usar e abusar destas 
funções.


Mudando a imagem quando clicar nela 
Para deixar sua página mais sofisticada, você pode utilizar um código 
Javascript que altera a imagem quando o usuário clica nela. 
O código abaixo mostra como você pode fazer isso:

<SCRIPT>
function change_img(source,index) {
document.images[index].src = source;
}
</SCRIPT>

<A HREF="JavaScript: change_img('figuraum.gif',0)">
<IMG SRC="figuradois.gif"></A> 


Abrindo novas janelas 

Neste capítulo mostraremos como podem ser abertas novas janelas sobre 
uma janela contendo o documento principal. 

É importante não confundir esta forma de abrir janelas com a divisão 
da tela em várias partes, ou mesmo com a chamada de outras páginas. 
Para que não existam dúvidas, explicaremos um pouco sobre estes dois 
outros métodos. 

A divisão de uma tela em várias janelas contendo documentos diferentes 
é feita através do objeto FRAME do Html. Neste caso, a tela inteira é 
considerada como um FrameSet e cada parte em que ela for dividida é 
considerada como um Frame. Cada Frame é definido dentro do FrameSet 
através da especificação dos parâmetros: % da tela na vertical (cols), % 
da tela na horizontal (rows) e nome de cada frame. Uma vez criado o 
FrameSet poderemos abrir documentos distintos em cada Frame. Para fazer 
isto, acrescente ao link do documento a diretiva target=nome do frame.

Ex. 
<href="Eventos.htm" target="Principal"> 
Isto fará com que o arquivo html Eventos.htm seja aberto dentro do frame 
de nome Principal

A simples chamade de outras telas (documentos) é feita através do link 
para o documento desejado.

Ex. 
<href="Eventos.htm" > 
Isto fará com que o arquivo html Eventos.htm seja aberto em substituição 
a tela existente.

Bem, voltemos ao nosso caso que é a abertura de janelas sobre um documento. 
Isto é feito através de comandos JavaScript, que permitem: Abrir uma janela,
Abrir um documento dentro desta janela, Escrever o conteúdo da janela, Fechar 
a janela e Fechar o documento. 

Abrindo a Janela 

A sintaxe geral deste método é a seguinte:

Variavel = window.open ("Url", "Nome da janela", "Opções")

Onde: 
Variavel - Nome que será atribuido como propriedade da janela. 
Url - Endereço Internet onde a janela será aberta. Normalmente voce estará 
utilizando a sua 
própria Url, neste caso, preencha com "". 
Nome da Janela - É o nome que aparecerá no top da janela (Título) 
Opções - São as opções que definem as características da janela, quais sejam:

toolbar - Cria uma barra de ferramentas tipo "Back", "Forward", etc. 
location - Abre a barra de location do browse 
directories - Abre a barra de ferramentas tipo "What's New", "Handbook", etc. 
status - Abre uma barra de status no rodapé da janela 
scrollbar - Abre barras de rolamento vertical e horizontal 
menubar - Cria uma barra de menu tipo "File", "Edit", etc. 
resizable - Permite ao usuário redimencionar a janela 
width - Especifica a largura da janela, em pixels 
height - Especifica a altura da janela, em pixels 
Todas as opções (exceto width e height) são boleanas e podem ser setadas de 
duas formas. Exemplo: "toolbar" ou "toolbar=1") são a mesma coisa. Se nada 
for especificado, entende-se que todas as opções estão ligadas; Caso seja 
especificada qualquer opção, será entendido que estão ligadas apenas as 
opções informadas. 
As opções devem ser informadas separadas por vírgula, sem espaço entre elas.

Abrindo um Documento 

Para abrir um documento dentro da janela, deve ser utilizado o seguinte método: 
Variavel.document.open()

Onde "Variavel" é o nome da variável associada ao método window.open

Escrevendo no Documento 

Para escrever a tela no documento, deve ser utilizado o seguinte método: 
Variavel.document.write ("Comandos html, Comandos JavaScript, Textos, etc.")

Fechando a Janela 

Para fechar a janela, utilize o seguinte método: 
Variavel.document.write ("window.close()")

Fechando o Documento 

Para fechar o documento, utilize o seguinte método: 
Variavel.document.close ()

A seguir, apresentamos um exemplo no qual estamos abrindo um documento onde 
o usuário escolherá uma opção (Elógica ou Recife) e dará um Click em um 
botão (Nova Janela). Neste momento será aberta uma nova janela que conterá 
a foto escolhida pelo usuário e um botão que, ao receber o Click, fechará 
a janela.

Normalmente, qualquer href ou src dentro de uma página, por padrão, acessa 
o arquivo ou a imagem no mesmo diretório onde está a página atual, a menos 
que seja especificado um novo caminho (Path). 
No caso de abertura de uma nova janela, através do método window.open, as 
versões mais antigas dos browses não conseguem "ver" o Path, sendo necessária 
a completa informação do caminho (path) onde o arquivo ou imagem estão 
armazenados, em todas as chamadas dos comandos Html href ou src. 
Observe que na função estamos utilizando dois novos métodos:
navigator.appVersion para verificarmos a versão do browse que esta sendo 
utilizado document.location. para obtermos o Path da localização do arquivo 
Html que está correntemente em uso. 
No exemplo abaixo estamos, inicialmente, identificando a versão do browse.
Caso seja antiga, para não escrevermos todo o caminho a cada chamada e ainda, 
considerando que os arquivos chamados estão no mesmo diretório da página atual, 
estamos obtendo o Path do arquivo atual e eliminando o nome do arquivo que está 
na última referencia do Path. Quando fizermos a chamada das imagens (comando src) 
só será necessário a concatenação do nome do arquivo chamado com a raiz do path 
que, no exemplo, armazenamos na variável de nome Local.

<script> 
function Abrejanela(Opcao) { 
Versao = navigator.appVersion 
Versao = Versao.substring(0, 1)
Local = ""
if (Versao < 3) { 
Local = document.location 
UltLoc = Local.lastIndexOf("/") 
Local = Local.substring(0, UltLoc + 1)
}
// 
NovaJanela = window.open ("", "OutraJanela", "width=300,height=400") NovaJanela.document.open() 
NovaJanela.document.write ("<html><head><title>Nova Janela") 
NovaJanela.document.write ("</title></head><body bgcolor='white'>") NovaJanela.document.write ("<form>") 
if (Opcao == 1) 
{ NovaJanela.document.write ("<br>Logomarca Elogica<hr><br>") 
NovaJanela.document.write 
("<img width=200 height=200 src=" + Local + "Marcaelo.gif>") } 
else 
{ NovaJanela.document.write ("<br>Recife Alto Astral<hr><br>") 
NovaJanela.document.write 
("<img width=150 height=200 src=" + Local + "Recife.gif>") } 
// 
NovaJanela.document.write ("<br><hr><p></p></form>") 
NovaJanela.document.write ("<form><input type='button' name='Fecha'" + 
"value='Fecha Janela'" + "onclick='window.close()'>") 
NovaJanela.document.write ("</form></body></html>") 
NovaJanela.document.close() } 
</script> 

<body> 
<p></p> 
<p>Escolha a foto a ser apresentada na nova janela:</p> 
<form method="POST" name="Form1"> 
<p> 
<input type=radio name="Opcao" value="1" checked>Elogica 
<input type=radio name="Opcao" value="2">Recife 
</p> <p> 
<input type="button" name="Envia" value="Nova Janela" 
onclick="if (Form1.Opcao[0].checked == true) 
{Abrejanela(Form1.Opcao[0].value) } 
else 
{Abrejanela(Form1.Opcao[1].value) } "> 
</p> 
</form> 
</body> 

Abrindo janelas personalizadas 


A função window.open() serve para controlar a apresentação de janelas 
durante a navegação. Exemplo:

<HTML>
<HEAD><TITLE>WINDOW.OPEN()</HEAD></TITLE>
<script language = "JavaScript">
function Abrir()
{
window.open("Pag1.htm", "JANELA", "height = 300, width = 400");
}
<BODY OnLoad = "Abrir()">
</BODY>
</HTML>

A função Abrir() irá chamar uma nova janela contendo o arquivo Pag1.htm, 
cujo título será JANELA e de altura 300 pixels e largura 400 pixels. 
Se quisermos, podemos ainda colocar mais parâmetros na função window.open. 
. Adicionando barra de rolagem:

window.open("Pag1.htm", "JANELA", "scrollbars = yes");

. Adicionando barra de status:

window.open("Pag1.htm", "JANELA", "status=yes");

. Removendo barra de endereços:

window.open("Pag1.htm", "JANELA", "location = no");

. Removendo barra de ferramentas:

window.open("Pag1.htm", "JANELA", "toolbar = no");

. Adicionando barra de menu:

window.open("Pag1.htm", "JANELA", "menubar=yes");

Um pouco sobre os parâmetros left/screenX e top/screenY

As propriedades left e screenX definem em pixels, a posição em que a janela 
será aberta a partir do lado esquerdo da janela do browser. Você pode usar 
left para navegador Internet Explorer e screenX para Netscape.

As propriedades top e screenY definem em pixels, a posição em que a janela 
será aberta a partir do lado de cima da janela do browser. Você pode usar 
top para navegador Internet Explorer e screenY para Netscape.

Criando funções, Funções intrínsecas, Criando novas instâncias 
e Manipulando arrays 

Uma função é um set de instruções, que só devem ser executadas quando a 
função for acionada.

A sintaxe geral é a seguinte: 

function NomeFunção (Parâmetros) 
{ Ação }

Suponha uma função que tenha como objetivo informar se uma pessoa é maior 
ou menor de idade, recebendo como parâmetro a sua idade. 

function Idade (Anos) { 
if (Anos > 17) 
{ alert ("Maior de Idade") } 
else 
{ alert ("menor de Idade") } 
}

Para acionar esta função, suponha uma caixa de texto, em um formulário, 
na qual seja informada a idade e, a cada informação, a função seja acionada. 

<form> <input type=text size=2 maxlength=2 name="Tempo" onchange="Idade(Tempo.value)"> </form> Observe-se que o parâmetro passado (quando ocorre o evento "onchange") foi o conteúdo da caixa de texto "Tempo" (propriedade "value") e que, na função, chamamos de "Anos". Ou seja, não existe co-relação entre o nome da variável passada e a variável de recepção na função. Apenas o conteúdo é passado.

>> Funções intrínsecas

São funções embutidas na própria linguagem. A sintaxe geral é a seguinte: 

Result = função (informação a ser processada) 

eval = Calcula o conteúdo da string 
parseInt - Transforma string em inteiro 
parseFloat - Transforma string em número com ponto flutuante 
date() - Retorna a data e a hora (veja o capítulo manipulando datas) 

ex1: Result = eval ( " (10 * 20) + 2 - 8") 

ex2: Result = eval (string) 

No primeiro exemplo Result seria igual a 194. No segundo, depende do conteúdo 
da string, que também pode ser o conteúdo (value) de uma caixa de texto. 

- Funções tipicamente Matemáticas: 

Math.abs(número) - retorna o valor absoluto do número (ponto flutuante) 
Math.ceil(número) - retorna o próximo valor inteiro maior que o número 
Math.floor(número) - retorna o próximo valor inteiro menor que o número 
Math.round(número) - retorna o valor inteiro, arredondado, do número 
Math.pow(base, expoente) - retorna o cálculo do exponencial 
Math.max(número1, número2) - retorna o maior número dos dois fornecidos 
Math.min(número1, número2) - retorna o menor número dos dois fornecidos 
Math.sqrt(número) - retorna a raiz quadrada do número 
Math.SQRT2 - retorna a raiz quadrada de 2 (aproximadamente 1.414) 
Math.SQRT_2 - retorna a raiz quadrada de 1/2 (aproximadamente 0.707) 
Math.sin(número) - retorna o seno de um número (anglo em radianos) 
Math.asin(número) - retorna o arco seno de um número (em radianos) 
Math.cos(número) - retorna o cosseno de um número (anglo em radianos) 
Math.acos(número) - retorna o arco cosseno de um número (em radianos) 
Math.tan(número) - retorna a tangente de um número (anglo em radianos) 
Math.atan(número) - retorna o arco tangente de um número (em radianos) 
Math.pi retorna o valor de PI (aproximadamente 3.14159) 
Math.log(número) - retorna o logarítmo de um número 
Math.E - retorna a base dos logarítmos naturais (aproximadamente 2.718) 
Math.LN2 - retorna o valor do logarítmo de 2 (aproximadamente 0.693) 
Math.LOG2E - retorna a base do logarítmo de 2 (aproximadamente 1.442) 
Math.LN10 retorna o valor do logarítmo de 10 (aproximadamente 2.302) 
Math.LOG10E - retorna a base do logarítmo de 10 (aproximadamente 0.434) 

Observação: 
Em todas as funções, quando apresentamos a expressão "(número)", na verdade 
queremos nos referir a um argumento que será processado pela função e que 
poderá ser: um número, uma variável ou o conteúdo de um objeto (propriedade 
value). 

>> Criando novas instâncias

Através do operador new podem ser criadas novas instâncias a objetos já 
existentes, mudando o seu conteúdo, porém, mantendo suas propriedades. 

A sintaxe geral é a seguinte: 

NovoObjeto = new ObjetoExistente (parâmetros) 

Ex1. 
MinhaData = new Date () 

MinhaData passou a ser um objeto tipo Date, com o mesmo conteúdo existente 
em Date (data e hora atual) 

Ex2: 
MinhaData = new Date(1996, 05, 27) 

MinhaData passou a ser um objeto tipo Date, porém, com o conteúdo de uma 
nova data. 

Ex3: 
Suponha a existência do seguinte objeto chamado Empresas 

function Empresas (Emp, Nfunc, Prod) 
{ this.Emp = Emp 
this.Nfunc = Nfunc 
this.Prod = Prod }

Podemos criar novas instâncias, usando a mesma estrutura, da seguinte forma: 

Elogica = new Empresas("Elogica", "120", "Serviços") 
Pitaco = new Empresas("Pitaco", "35", "Software") 
Corisco = new Empresas("Corisco", "42", "Conectividade") 

Assim, a variável Elogica.Nfunc terá o seu conteúdo igual a 120 

>> Manipulando arrays

O JavaScript não tem um tipo de dado ou objeto para manipular arrays. 
Por isso, para trabalhar com arrays é necessário a criação de um objeto 
com a propriedade de criação de um array. 

No exemplo abaixo, criaremos um objeto tipo array de tamanho variável e 
com a função de "limpar" o conteúdo das variáveis cada vez que uma nova 
instância seja criada a partir dele. 

function CriaArray (n) { 
this.length = n 
for (var i = 1 ; i <= n ; i++) 
{ this[i] = "" } } 

Agora podemos criar novas instâncias do objeto "CriaArray" e alimentá-los 
com os dados necessários.

NomeDia = new CriaArray(7) 
NomeDia[0] = "Domingo" 
NomeDia[1] = "Segunda" 
NomeDia [2] = "Terça" 
NomeDia[3] = "Quarta" 
NomeDia[4] = "Quinta" 
NomeDia[5] = "Sexta" 
NomeDia[6] = "Sábado" 

Atividade = new CriaArray(5) 
Atividade[0] = "Analista" 
Atividade[1] = "Programador" 
Atividade[2] = "Operador" 
Atividade[3] = "Conferente" 
Atividade[4] = "Digitador" 

Agora poderemos obter os dados diretamente dos arrays. 

DiaSemana = NomeDia[4] 
Ocupação = Atividade[1] 

DiaSemana passaria a conter Quinta e Ocupação conteria Programador.

Outra forma de se trabalhar com arrays é criar novas instâncias dentro do 
próprio objeto do array, o que proporciona o mesmo efeito de se trabalhar 
com matriz. Isso pode ser feito da seguinte forma: 

function Empresas (Emp, Nfunc, Prod) { 
this.Emp = Emp 
this.Nfunc = Nfunc 
this.Prod = Prod }

TabEmp = new Empresas(3) 
TabEmp[1] = new Empresas("Elogica", "120", "Serviços") 
TabEmp[2] = new Empresas("Pitaco", "35", "Software") 
TabEmp[3] = new Empresas("Corisco", "42", "Conectividade")

Assim, poderemos obter a atividade da empresa número 3, cuja resposta seria 
Conectividade, da seguinte forma: 

Atividade = TabEmp[3].Prod 

Obs.: É importante lembrar que, embora os exemplos estejam com indexadores 
fixos, os indexadores podem ser referências ao conteúdo de variáveis. 

Eventos, Criando variáveis, Escrevendo no documento e Mensagens 


>> Eventos

São fatos que ocorrem durante a execução do sistema, a partir dos quais 
o programador pode definir ações a serem realizadas pelo programa. 
Abaixo apresentamos a lista dos eventos possíveis, indicando os momentos 
em que os mesmos podem ocorrer, bem como, os objetos passíveis de sua 
ocorrência.

onload - Ocorre na carga do documento. Ou seja, só ocorre no BODY do documento.
onunload - Ocorre na descarga (saída) do documento. Também só ocorre no BODY. 
onchange - Ocorre quando o objeto perde o focus e houve mudança de conteúdo. 
           válido para os objetos Text, Select e Textarea. 
onblur - Ocorre quando o objeto perde o focus, independente de ter havido 
         mudança. válido para os objetos Text, Select e Textarea. 
onfocus - Ocorre quando o objeto recebe o focus. válido para os objetos 
          Text, Select e Textarea. 
onclick - Ocorre quando o objeto recebe um Click do Mouse. válido para os 
          objetos Buton, Checkbox, Radio, Link, Reset e Submit. 
onmouseover - Ocorre quando o ponteiro do mouse passa por sobre o objeto. 
              válido apenas para Link. 
onselect - Ocorre quando o objeto é selecionado. válido para os objetos 
           Text e Textarea. 
onsubmit - Ocorre quando um botão tipo Submit recebe um click do mouse. 
           válido apenas para o Form. 

>> Criando variáveis

A variável é criada automaticamente, pela simples associação de valores a mesma.

Ex. NovaVariavel = "Jose" 

Foi criada a variável de nome NovaVariavel que, passou a conter a string Jose. 

As variáveis podem ser Locais ou Globais. As variáveis que são criadas dentro 
de uma função são Locais e referenciáveis apenas dentro da função. 
As variáveis criadas fora de funções são Globais, podendo serem referenciadas 
em qualquer parte do documento. 

Desta forma, variáveis que precisam ser referenciadas por várias funções ou em 
outra parte do documento, precisam ser definidas como globais.

Embora não seja recomendável, em uma função, pode ser definida uma variável 
local com o mesmo nome de uma variável global. Para isso utiliza-se o método 
de definição var.

Ex. Variável Global : MinhaVariavel = "" 
Variável Local : var MinhaVariavel = "" 

>> Escrevendo no documento

O JavaScript permite que o programador escreva linhas dentro de uma página 
(documento), através do método write. As linhas escritas desta forma, podem 
conter textos, expressões JavaScript e comandos Html. 
As linhas escritas através deste método aparecerão no ponto da tela onde o 
comando for inserido.

Ex: 
<script> 
valor = 30 
document.write ("Minha primeira linha") 
document.write ("Nesta linha aparecerá o resultado de : " + (10 * 10 + valor)) 
</script>

A idéia do exemplo acima é escrever duas linhas. Entretanto o método write 
não insere mudança de linha, o que provocará o aparecimento de apenas uma 
linha com os dois textos emendados. 
Para evitar este tipo de ocorrência, existe o método writeln que escreve 
uma linha e espaceja para a seguinte. Entretanto, em nossos testes, este 
comando não surtiu efeito,obtendo-se o mesmo resultado do método write. 
A solução encontrada para esta situação foi a utilização do comando de mudança 
de parágrafo da linguagem Html.

Ex: 
<script> 
valor = 30 
document.write ("<p>Minha primeira linha</p>") 
document.write ("<p>Nesta linha aparecerá o resultado de : " + (10 * 10 + valor) + "</p>") 
</script>

Isto resolve a questão da mudança de linha, porém, vai gerar uma linha em 
branco, entre cada linha, por se tratar de mudança de parágrafo. 
Caso não seja desejado a existência da linha em branco, a alternativa é 
utilizar o comando Html <br> que apenas muda de linha.

Ex: 
<script> 
valor = 30 
document.write ("<br>Minha primeira linha") 
document.write ("<br>Nesta linha aparecerá o resultado de : " + (10 * 10 + valor) ) 
</script>

>> Mensagens

Existem três formas de comunicação com o usuário através de mensagens. 

Apenas Observação: 

alert ( mensagem ) 

Ex. 
alert ("Certifique-se de que as informações estão corretas") 

Mensagem que retorna confirmação de OK ou CANCELAR: 

confirm (mensagem) 

Ex. 
if (confirm ("Algo está errado...devo continuar??")) 
{ alert("Continuando") } 
else 
{ alert("Parando") }

Recebe mensagem via caixa de texto Input: 

Receptor = prompt ("Minha mensagem", "Meu texto") 

Onde: 
Receptor é o campo que vai receber a informação digitada pelo usuário 
Minha mensagem é a mensagem que vai aparecer como Label da caixa de input 
Meu texto é um texto, opcional, que aparecerá na linha de digitação do usuário 

Ex. 
Entrada = prompt("Informe uma expressão matemática", "") 
Resultado = eval(Entrada) 
document.write("O resultado é = " + Resultado) 

Manipulando strings, Manipulando datas, Interagindo com o 
usuário e Objeto input TEXT 


>> Manipulando string's

O JavaScript é bastante poderoso no manuseio de String´s, fornecendo 
ao programador uma total flexibilidade em seu manuseio. 
Abaixo apresentamos os métodos disponíveis para manuseio de string´s.

string.length - retorna o tamanho da string (quantidade de bytes)
string.charAt(posição) - retorna o caracter da posição especificada 
                         (inicia em 0) 
string.indexOf("string") - retorna o número da posição onde começa a 
                           primeira "string" 
string.lastindexOf("string") - retorna o número da posição onde começa 
                               a última "string" 
string.substring(index1, index2) - retorna o conteúdo da string que 
                                   corresponde ao intervalo especificado. 
Começando no caracter posicionado em index1 e terminando no caracter 
imediatamente anterior ao valor especificado em index2. 

Ex. 
Todo = "Elogica" 
Parte = Todo.substring(1, 4) 

(A variável Parte receberá a palavra log) 

string.toUpperCase() - Transforma o conteúdo da string para maiúsculo (Caixa Alta)
string.toLowerCase() - Transforma o conteúdo da string para minúsculo (Caixa Baixa) 
escape ("string") - retorna o valor ASCII da string (vem precedido de %) 
unscape("string") - retorna o caracter a partir de um valor ASCII (precedido de %) 

>> Manipulando datas

Existe apenas uma função para que se possa obter a data e a hora. 
É a função Date(). Esta função devolve data e hora no formato:Dia da 
semana, Nome do mês, Dia do mês, Hora:Minuto:Segundo e Ano 

Ex. 
Fri May 24 16:58:02 1996 

Para se obter os dados separadamente, existem os seguintes métodos: 

getDate() - Obtém o dia do mês (numérico de 1 a 31) 
getDay() - Obtém o dia da semana (0 a 6) 
getMonth() - Obtém o mês (numérico de 0 a 11) 
getYear() - Obtém o ano 
getHours() - Obtém a hora (numérico de 0 a 23) 
getMinutes() - Obtém os minutos (numérico de 0 a 59) 
getSeconds() - Obtém os segundos (numérico de 0 a 59)

No exemplo abaixo obteremos o dia da semana. Para tal, utilizaremos a 
variável DataToda para armazenar data/hora e a variável DiaHoje para 
armazenar o número do dia da semana.

DataToda = new Date() 
DiaHoje = DataToda.getDay() 

Para obter o dia da semana alfa, teremos que construir uma tabela com os 
dias da semana e utilizar a variável DiaHoje como indexador. 

function CriaTab (n) { 
this.length = n 
for (var x = 1 ; x<= n ; x++) 
{ this[x] = "" } } 

NomeDia = new CriaTab(7) 
NomeDia[0] = "Domingo" 
NomeDia[1] = "Segunda" 
NomeDia [2] = "Terça" 
NomeDia[3] = "Quarta" 
NomeDia[4] = "Quinta" 
NomeDia[5] = "Sexta" 
NomeDia[6] = "Sábado" 

DiaSemana = NomeDia[DiaHoje] 

Para criar uma variável tipo Date com o conteúdo informado pela aplicação, 
existe o método set. Assim, temos os seguintes métodos: setDate, setDay, 
setMonth, setYear, setHours, setMinutes e setSeconds. 
Seguindo o exemplo acima, para mudar o mês para novembro, teríamos: 

DataToda.setMonth(10) 

Exemplos adicionais serão encontrados no capítulo "Usando Timer e Date".

>> Interagindo com o usuário

A interação com o usuário se dá através de objetos para entrada de dados 
(textos), marcação de opções (radio, checkbox e combo), botões e link's 
para outras páginas.

Conceitualmente, os objetos são divididos em: Input, Textarea e Select. 

O objeto Input divide-se (propriedade Type) em: 

Password 
Text 
Hidden 
Checkbox 
Radio 
Button 
Reset 
Submit 

A construção destes objetos é feita pela linguagem HTML (HiperText Mark-up 
Language). Portanto, é aconselhável que sejam criados utilizando-se 
ferramentas de geração de páginas HTML, como o HotDog ou, mais recomendado, 
FrontPage. 

>> Objeto input TEXT

É o principal objeto para entrada de dados. 
Suas principais propriedades são: type, size, maxlength, name e value. 

type=text : Especifica um campo para entrada de dados normal 
size : Especifica o tamanho do campo na tela. 
maxlength : Especifica a quantidade máxima de caracteres permitidos. 
name : Especifica o nome do objeto 
value : Armazena o conteúdo do campo. 

Os eventos associados a este objeto são: onchange, onblur, onfocus e onselect. 

Ex: 
<form name="TText"> 
<p>Entrada de Texto <input type=text size=20 maxlength=30 name="CxTexto" value="" onchange="alert ('Voce digitou ' + CxTexto.value)"> 
</p> 
</form> 

Página de abertura 
<HTML>
<HEAD>
<TITLE>Coloque o título aqui</TITLE>
<SCRIPT LANGUAGE = "JavaScript">
function makeArray() {
this.length = makeArray.arguments.length;
for (var i = 0; i < this.length; i++)
this[i + 1] = makeArray.arguments[i];
} 

function makeSlideShow (obj, wait, pre, url) {
this.curText = '';
this.posit = 1; 
this.word = obj; 
this.length = obj.length;
this.pre = pre;
this.wait = wait;
this.url = url;
this.display = displaySlideShow;
} 

function displaySlideShow() {
if (this.posit <= this.length) {
this.curText = this.word[this.posit]
outStringWord = blankFrameTop + this.pre + this.curText + blankFrameBottom;
parent.draw.location = 'javascript:parent.outStringWord';
this.posit++;
}
else {
doneLoop = true;
top.location = this.url;
}
} 

function displayLoop() {
if (!doneLoop) reDraw = setTimeout('displayLoop()', wordIntro.wait);
wordIntro.display();
} 

var words = new makeArray ('Coloque o primeiro texto aqui', 'Coloque o segundo texto aqui', 'Coloque o terceiro texto aqui', 'Coloque o quarto texto aqui', 'ABRINDO O SITE... ');
var wordIntro = new makeSlideShow (words, 2500, '<CENTER><BR><BR><BR><BR><BR><BR><BR><BR><FONT SIZE = 5>', 'Coloque aqui o URL da página que vai aparecer');
var blankFrameTop = '<HTML><BODY BGCOLOR = "#000000" TEXT = "#FFFFFF">';
var blankFrameBottom = '</BODY></HTML>';
var blankFrame = blankFrameTop + blankFrameBottom;
var doneLoop = false;

</SCRIPT>
</HEAD><FRAMESET onLoad = "displayLoop()" ROWS = "100%, *" FRAMEBORDER = NO BORDER = 0> 

<FRAME 
SCROLLING=AUTO
SRC = "javascript:parent.blankFrame"
NAME = "draw" 
MARGINWIDTH = 2 
MARGINHEIGHT = 2>
</FRAMESET> 
</HTML> 


Efeito de abertura 

Cole em sua página HTML o código abaixo.

<style>
<!--
.entrada{
position:absolute;
left:0;
top:0;
layer-background-color:green;
background-color:green;
border:0.1px solid green
}
-->
</style>
<div id="i1" class="entrada"></div>
<div id="i2" class="entrada"></div>
<div id="i3" class="entrada"></div>
<div id="i4" class="entrada"></div>
<div id="i5" class="entrada"></div>
<div id="i6" class="entrada"></div>
<div id="i7" class="entrada"></div>
<div id="i8" class="entrada"></div>
<script language="JavaScript1.2">
//
var velocidade=10
var tempo=new Array()
var tempo2=new Array()
if (document.layers){
for (i=1;i<=8;i++){
tempo[i]=eval("document.i"+i+".clip")
tempo2[i]=eval("document.i"+i)
tempo[i].width=window.innerWidth/8-0.3
tempo[i].height=window.innerHeight
tempo2[i].left=(i-1)*tempo[i].width
}
}
else if (document.all){
var clipbottom=document.body.offsetHeight,cliptop=0
for (i=1;i<=8;i++){
tempo[i]=eval("document.all.i"+i+".style")
tempo[i].width=document.body.clientWidth/8
tempo[i].height=document.body.offsetHeight
tempo[i].left=(i-1)*parseInt(tempo[i].width)
}
}
function openit(){
window.scrollTo(0,0)
if (document.layers){
for (i=1;i<=8;i=i+2)
tempo[i].bottom-=velocidade
for (i=2;i<=8;i=i+2)
tempo[i].top+=velocidade
if (tempo[2].top>window.innerHeight)
clearInterval(stopit)
}
else if (document.all){
clipbottom-=velocidade
for (i=1;i<=8;i=i+2){
tempo[i].clip="rect(0 auto+"+clipbottom+" 0)"
}
cliptop+=velocidade
for (i=2;i<=8;i=i+2){
tempo[i].clip="rect("+cliptop+" auto auto)"
}
if (clipbottom<=0)
clearInterval(stopit)
}
}
function gogo(){
stopit=setInterval("openit()",100)
}
gogo()
</script>

- Mudando a cor do efeito 

Não gostou da cor verde? Então você pode mudar para uma outra cor qualquer. 
É muito simples! Veja: 

Repare que existe um trecho do código que contém: 

<style>
<!--
.entrada{
position:absolute;
left:0;
top:0;
layer-background-color:green;
background-color:green;
border:0.1px solid green
}
-->
</style> 

Onde se lê "green" (verde em inglês), você deve colocar o nome em inglês ou 
o código hexadecimal da cor que deseja. 

Você pode também selecionar uma das cores abaixo e substituir o trecho que 
citamos do código original por este novo que você receber. 


Efeito de abertura múltiplo 

Cole em sua página HTML o código abaixo.

<style>
.intro{position:absolute;
left:0;
top:0;
layer-background-color:green;
background-color:green;
border:0.1px solid green
}
</style> 

<div id="i1" class="intro"></div>
<div id="i2" class="intro"></div>
<div id="i3" class="intro"></div>
<div id="i4" class="intro"></div>
<div id="i5" class="intro"></div>
<div id="i6" class="intro"></div>
<div id="i7" class="intro"></div>
<div id="i8" class="intro"></div>
<script language="JavaScript1.2">
var velocidade=20
var tempo=new Array()
var tempo2=new Array()
if (document.layers){
for (i=1;i<=8;i++){
tempo[i]=eval("document.i"+i+".clip")
tempo2[i]=eval("document.i"+i)
tempo[i].width=window.innerWidth
tempo[i].height=window.innerHeight/8
tempo2[i].top=(i-1)*tempo[i].height
}
}
else if (document.all){
var clipright=document.body.clientWidth,clipleft=0
for (i=1;i<=8;i++){
tempo[i]=eval("document.all.i"+i+".style")
tempo[i].width=document.body.clientWidth
tempo[i].height=document.body.offsetHeight/8
tempo[i].top=(i-1)*parseInt(tempo[i].height)
}
}
function openit(){
window.scrollTo(0,0)
if (document.layers){
for (i=1;i<=8;i=i+2)
tempo[i].right-=velocidade
for (i=2;i<=8;i=i+2)
tempo[i].left+=velocidade
if (tempo[2].left>window.innerWidth)
clearInterval(stopit)
}
else if (document.all){
clipright-=velocidade
for (i=1;i<=8;i=i+2){
tempo[i].clip="rect(0 "+clipright+" auto 0)"
}
clipleft+=velocidade
for (i=2;i<=8;i=i+2){
tempo[i].clip="rect(0 auto auto "+clipleft+")"
}
if (clipright<=0)
clearInterval(stopit)
}
}
function gogo(){
stopit=setInterval("openit()",100)
}
gogo()
</script> 

- Mudando a cor do efeito 

Não gostou da cor verde? Então você pode mudar para uma outra cor qualquer. 
É muito simples! Veja: 

Repare que existe um trecho do código que contém: 

<style>
<!--
.entrada{
position:absolute;
left:0;
top:0;
layer-background-color:green;
background-color:green;
border:0.1px solid green
}
-->
</style> 

Onde se lê "green" (verde em inglês), você deve colocar o nome em inglês 
ou o código hexadecimal da cor que deseja. 

Você pode também selecionar uma das cores abaixo e substituir o trecho que 
citamos do código original por este novo que você receber. 


Efeito de abertura horizontal 

Cole em sua página HTML o código abaixo.

<style>
.intro{
position:absolute;
left:0;
top:0;
layer-background-color:black;
background-color:black;
border:0.1px solid black
}
</style>

<div id="i1" class="intro" style="background-color: #000000"></div>
<div id="i2" class="intro" style="background-color: #000000"></div> 

<script language="JavaScript1.2">
var velocidade=10 //Quanto maior o valor, maior a velocidade aqui
var tempo=new Array()
var tempo2=new Array()
if (document.layers){
for (i=1;i<=2;i++){
tempo[i]=eval("document.i"+i+".clip")
tempo2[i]=eval("document.i"+i)
tempo[i].width=window.innerWidth/2
tempo[i].height=window.innerHeight
tempo2[i].left=(i-1)*tempo[i].width
}
}
else if (document.all){
var clipright=document.body.clientWidth/2,clipleft=0
for (i=1;i<=2;i++){
tempo[i]=eval("document.all.i"+i+".style")
tempo[i].width=document.body.clientWidth/2
tempo[i].height=document.body.offsetHeight
tempo[i].left=(i-1)*parseInt(tempo[i].width)
}
}
function openit(){
window.scrollTo(0,0)
if (document.layers){
tempo[1].right-=velocidade
tempo[2].left+=velocidade
if (tempo[2].left>window.innerWidth/2)
clearInterval(stopit)
}
else if (document.all){
clipright-=velocidade
tempo[1].clip="rect(0 "+clipright+" auto 0)"
clipleft+=velocidade
tempo[2].clip="rect(0 auto auto "+clipleft+")"
if (clipright<=0)
clearInterval(stopit)
}
}
function gogo(){
stopit=setInterval("openit()",100)
}
gogo()
</script> 

- Mudando a cor do efeito 

Não gostou da cor verde? Então você pode mudar para uma outra cor qualquer. 
É muito simples! Veja: 

Repare que existe um trecho do código que contém: 

<style>
<!--
.entrada{
position:absolute;
left:0;
top:0;
layer-background-color:green;
background-color:green;
border:0.1px solid green
}
-->
</style> 

Onde se lê "green" (verde em inglês), você deve colocar o nome em inglês ou 
o código hexadecimal da cor que deseja. 

Você pode também selecionar uma das cores abaixo e substituir o trecho que 
citamos do código original por este novo que você receber. 


Marca dágua 

<div id="marca" style="position:absolute; visibility:show; left:225px; top:-100px; z-index:2">
<IMG SRC="Coloque aqui o nome da sua imagem" border=0></div>
<script language=javaScript>
function checanav(){
var x = navigator.appVersion;
y = x.substring(0,4);
if (y>=4) variaveis(),local();
}
function variaveis(){
if (navigator.appName == "Netscape") {
h=".left";
v=".top";
dS="document.";
sD="";
iW="window.innerWidth"
iH="window.innerHeight"
osX="window.pageXOffset"
osY="window.pageYOffset"
}else{
h=".pixelLeft";
v=".pixelTop";
dS="";
sD=".style";
iW="document.body.clientWidth"
iH="document.body.clientHeight"
osX="document.body.scrollLeft"
osY="document.body.scrollTop"
}
}
obW=99+30
obH=24+40
objectXY="marca" 

function local(){
eval(dS+objectXY+sD+h+"="+((eval(iW))-obW+(eval(osX))));
eval(dS+objectXY+sD+v+"="+((eval(iH))-obH+(eval(osY))));
setTimeout("local()",100)
}
checanav()
</script> 


Imagem voando pela tela 

Lembre-se de que a imagem deve estar no mesmo diretório da sua página HTML.

<SCRIPT language="JavaScript1.2"> 
var imagem="Coloque o nome do arquivo de imagem aqui"
if (document.layers)
{document.write("<LAYER NAME='animacao' LEFT=10 TOP=10><img src='"+imagem+"' ></LAYER>")}
else if (document.all){document.write("<div id='animacao' style='position:absolute; top:10px; left:10px; width:17px; height:22px; z-index:50'> <img src='"+imagem+"'> </div>")} 

conta=-1; 
move=1;
function curva(){
abc=new Array(0,1,1,1,2,3,4,0,6,-1,-1,-1,-2,-3,-4,0,-6)
for (i=0; i < abc.length; i++)
{var C=Math.round(Math.random()*[i])}
iniciar=abc[C];
setTimeout('curva()',1900);
return iniciar;
}
ypos=10;
xpos=10;
movimento = 60;
function moveR(){
caminho=movimento+=iniciar;
y = 4*Math.sin(caminho*Math.PI/180);
x = 6*Math.cos(caminho*Math.PI/180);
if (document.layers){
ypos+=y;
xpos+=x;
document.animacao.top=ypos+window.pageYOffset;
document.animacao.left=xpos+window.pageXOffset;
}
else if (document.all){
ypos+=y;
xpos+=x;
document.all.animacao.style.top=ypos+document.body.scrollTop;
document.all.animacao.style.left=xpos+document.body.scrollLeft;
}
T=setTimeout('moveR()',50);
}
function edges(){
if (document.layers){
if (document.animacao.left >= window.innerWidth-40+window.pageXOffset)movimento=Math.round(Math.random()*45+157.5);
if (document.animacao.top >= window.innerHeight-30+window.pageYOffset)movimento=Math.round(Math.random()*45-112.5);
if (document.animacao.top <= 2+window.pageYOffset) movimento = Math.round(Math.random()*45+67.5);//OK!
if (document.animacao.left <= 2+window.pageXOffset) movimento = Math.round(Math.random()*45-22.5);//OK!
}
else if (document.all)
{
if (document.all.animacao.style.pixelLeft >= document.body.offsetWidth-45+document.body.scrollLeft)movimento=Math.round(Math.random()*45+157.5);
if (document.all.animacao.style.pixelTop >= document.body.offsetHeight-35+document.body.scrollTop)movimento=Math.round(Math.random()*45-112.5);
if (document.all.animacao.style.pixelTop <= 2+document.body.scrollTop) movimento = Math.round(Math.random()*45+67.5);//OK!
if (document.all.animacao.style.pixelLeft <= 2+document.body.scrollLeft) movimento = Math.round(Math.random()*45-22.5);//OK!
}
setTimeout('edges()',100);
}
function efeito(){
curva();
moveR();// onUnload="opener.gO()"
edges();
}
if (document.all||document.layers)
efeito()
</script> 



Janela animada 
Copie o código a seguir e cole-o em suas páginas abaixo da tag <Body>.

<SCRIPT LANGUAGE="JavaScript">
function janelanimada(endereco) {
var velocidadev = 5; 
var topo = 0;
var esquerda = 0;
var velocidadeh = 8;
if (document.all) {
var tamlar = window.screen.availHeight - topo;
var tamjan = window.screen.availWidth - esquerda;
var tamanho = window.open("","","left=" + esquerda + ",top=" + topo + ",width=1,height=1,scrollbars=yes");
for (sizeheight = 1; sizeheight < tamlar; sizeheight += velocidadev) {
tamanho.resizeTo("1",sizeheight );}
for (sizewidth = 1; sizewidth < tamjan; sizewidth += velocidadeh) {
tamanho.resizeTo(sizewidth+6,sizeheight );}
tamanho.location = endereco;}
else
window.location = endereco;}
</script>
<a href="http://link1" onClick="janelanimada('http://link1');return false;">link1</a><br>
<a href="http://link2" onClick="janelanimada('http://link2');return false;">link2</a><br>

Obs: Este exemplo é para 2 tipos de links. 


Habilitar som na página 

<script language="JavaScript">
<!--
function tocar () {
document.write('<embed src=NOME DO ARQUIVO DE MÚSICA AQUI width=1 height=1 autostart=true LOOP=1000></embed>');
}
function cancelar () {
}
// -->
if(confirm("Este site possui música de fundo, deseja ouvir?")) {
tocar();
}
else {
cancelar()
}
</script> 


Links com descrição 

Inclua este código abaixo da tab <body> em sua página. 

<SCRIPT LANGUAGE="JavaScript">
<!--
if (!document.layers&&!document.all)
event="test";
function Mostra(current,e,text)
{
if (document.all)
{
titulo=text.split('<BR>')
if (titulo.length>1)
{
titulos=''
for (i=0;i<titulo.length;i++)
titulos+=titulo[i]
current.title=titulos
}
else
current.title=text
}
else if (document.layers){
document.texto.document.write('<layer bgColor="white" style="border:1px solid black;font-size:15px;font-family:Arial;font-size:9pt;font-weight:bold">'+text+'</layer>')
document.texto.document.close()
document.texto.left=e.pageX+5
document.texto.top=e.pageY+5
document.texto.visibility="show"
}
}
function Esconde(){
if (document.layers)
document.texto.visibility="hidden"
}
//-->
</SCRIPT>
<DIV ID="texto" STYLE="POSITION: absolute; VISIBILITY: hidden"></DIV> - Passo 2: 

Agora você deve incluir em cada link a descrição que vai aparecer. 
Inclua este parâmetro em cada tag de link: 

onMouseOver="Mostra(this,event,'Coloque a mensagem aqui')" onMouseOut="Esconde()" 

Exemplo:
Seu link é assim:
<a href=fotos.html>Fotos</a> 

E fica assim:
<a href=fotos.html onMouseOver="Mostra(this,event,'Novas fotos da viagem que fiz este ano!')" onMouseOut="Esconde()">Fotos</a> 


Voltar